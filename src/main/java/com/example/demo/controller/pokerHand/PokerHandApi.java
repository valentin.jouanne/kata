package com.example.demo.controller.pokerHand;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface PokerHandApi {
    @ApiOperation(value = "Return pokerHand result", notes = "This endpoint returns pokerHand result ", response = Integer.class, tags = {"Poker",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Response containing pokerHand result", response = Integer.class),
            @ApiResponse(code = 500, message = "Unexpected error", response = Integer.class)})
    @RequestMapping(value = "/poker/{hand}", method = RequestMethod.GET)
    String getResult(@PathVariable("hand") String hand);
}
