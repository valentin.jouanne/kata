package com.example.demo.controller.pokerHand;

import com.example.demo.controller.DiceApi;
import com.example.demo.model.Dice;
import com.example.demo.model.pokerHand.PokerHand;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PokerHandController implements PokerHandApi {


    @Override
    public String getResult(String hand) {
        return new PokerHand().handResult(hand);
    }
}
