package com.example.demo.controller;

import com.example.demo.model.Dice;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameController implements DiceApi {

    @Override
    public int getRoll(@PathVariable("value") int value) {
        return new Dice(value).roll();
    }

    @Override
    public int getExplosiveRoll(@PathVariable("value") int value) {
        return new Dice(value, true).roll();
    }
}
