package com.example.demo.controller.lifeGame;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface LifeGameAPI {
    @ApiOperation(value = "Return life game result", notes = "This endpoint returns life game result ", response = Integer.class, tags = {"Life Game",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Response containing pokerHand result", response = Integer.class),
            @ApiResponse(code = 500, message = "Unexpected error", response = Integer.class)})
    @RequestMapping(value = "/life_game/", method = RequestMethod.GET)
    String getNextGen(@RequestBody String generation0);
}
