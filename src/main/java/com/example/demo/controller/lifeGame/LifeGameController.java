package com.example.demo.controller.lifeGame;

import com.example.demo.model.lifeGame.LifeGame;
import com.example.demo.model.pokerHand.PokerHand;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LifeGameController implements LifeGameAPI {

    @Override
    public String getNextGen(String generation0) {
        return new LifeGame(generation0).nextGen();
    }
}
