package com.example.demo.controller.diamond;

import com.example.demo.model.diamond.Diamond;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

public interface DiamondApi {
    @ApiOperation(value = "Return diamond result", notes = "This endpoint returns diamond ", response = Integer.class, tags = {"Diamond",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Response containing the dice result", response = Integer.class),
            @ApiResponse(code = 500, message = "Unexpected error", response = Integer.class)})
    @RequestMapping(value = "/diamond/{letter}", method = RequestMethod.GET)
    String getDiamond(@PathVariable("letter") Character letter);
}
