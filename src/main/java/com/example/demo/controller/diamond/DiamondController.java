package com.example.demo.controller.diamond;

import com.example.demo.model.diamond.Diamond;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DiamondController implements DiamondApi {

    @Override
    public String getDiamond(Character letter) {
        return new Diamond().of(letter);
    }
}
