package com.example.demo.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface DiceApi {
    @ApiOperation(value = "Return the current dice result", notes = "This endpoint returns dice result ", response = Integer.class, tags = {"Dice",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Response containing the dice result", response = Integer.class),
            @ApiResponse(code = 500, message = "Unexpected error", response = Integer.class)})
    @RequestMapping(value = "/roll/d{value}", method = RequestMethod.GET)
    int getRoll(@PathVariable("value") int value);

    @ApiOperation(value = "Return the current explosive dice result", notes = "This endpoint returns explosive dice result ", response = Integer.class, tags = {"Dice",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Response containing the explosive dice result", response = Integer.class),
            @ApiResponse(code = 500, message = "Unexpected error", response = Integer.class)})
    @RequestMapping(value = "/roll/explosive/d{value}", method = RequestMethod.GET)
    int getExplosiveRoll(@PathVariable("value") int value);
}
