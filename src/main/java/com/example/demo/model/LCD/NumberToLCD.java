package com.example.demo.model.LCD;


import com.example.demo.model.pokerHand.Card;
import com.example.demo.model.pokerHand.Combination;

import java.util.List;

public class NumberToLCD {


    public static final String NEW_LINE = "\n";
    public static final String OFF = " ";
    public static final String V_ON = "|";
    public static final String H_ON = "_";

    public String convert(int nbr) {
        StringBuilder lcd = new StringBuilder();
        firstLine(nbr, lcd);
        secondLine(nbr, lcd);
        thirdLine(nbr, lcd);
        return lcd.toString();
    }

    private void firstLine(int nbr, StringBuilder lcd) {
        lcd.append(OFF);
        lcd.append(Led1(nbr));
        lcd.append(OFF);
        lcd.append(NEW_LINE);
    }

    private void secondLine(int nbr, StringBuilder lcd) {
        lcd.append(led2(nbr));
        lcd.append(led3(nbr));
        lcd.append(led4(nbr));
        lcd.append(NEW_LINE);
    }

    private void thirdLine(int nbr, StringBuilder lcd) {
        lcd.append(led5(nbr));
        lcd.append(led6(nbr));
        lcd.append(led7(nbr));
    }

    private String Led1(int nbr) {
        if (nbr != 1 && nbr != 4) {
            return H_ON;
        }
        return OFF;
    }

    private String led2(int nbr) {
        if (nbr == 0 || nbr > 3 && nbr != 7) {
            return V_ON;
        }
        return OFF;
    }

    private String led3(int nbr) {
        if (nbr > 1 && nbr != 7) {
            return H_ON;
        }
        return OFF;
    }

    private String led4(int nbr) {
        if (nbr != 5 && nbr != 6) {
            return V_ON;
        }
        return OFF;
    }

    private String led5(int nbr) {
        if (nbr % 2 == 0 && nbr != 4) {
            return V_ON;
        }
        return OFF;
    }

    private String led6(int nbr) {
        if (nbr != 1 && nbr != 4 && nbr != 7) {
            return H_ON;
        }
        return OFF;
    }

    private String led7(int nbr) {
        if (nbr != 2) {
            return V_ON;
        }
        return OFF;
    }
//
//    public enum Led {
//        led2(V_ON),
//        led3(H_ON),
//        led4(V_ON),
//        led5(V_ON),
//        led6(H_ON),
//        led7(V_ON);
//        private String value;
//        private LCDStrategy lcdStrategy;
//        Operation(final LCDStrategy lcdStrategy) {
//            this.lcdStrategy = lcdStrategy;
//        }
//        @Override
//        public boolean condition(Double x, Double y) {
//            return lcdStrategy.compute(x, y);
//        }
//
//        Led(String value) {
//            this.value = value;
//        }
//
//        public String getValue() {
//            return value;
//        }
//
//    }

    public static void main(String[] args) {
        NumberToLCD lcd = new NumberToLCD();
        System.out.println(lcd.convert(0));
    }
}
