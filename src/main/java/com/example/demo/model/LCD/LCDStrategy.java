package com.example.demo.model.LCD;

@FunctionalInterface
public interface LCDStrategy {
    <T> void compute(T x, T y);
}
