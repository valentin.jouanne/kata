package com.example.demo.model.syllabes;

public class Syllabes {
    public String separate(String word) {
        if (word == null) {
            return null;
        }
        if (word.length() <= 4) {
            return word;
        }
        StringBuilder stringBuilder = new StringBuilder(word);
        for (int index = 0; index < word.length() - 1; index++) {
            if (isVowelFollowedBy2Consonant(word, index) && !isFollowedByLastLetter(word, index+1)) {
                stringBuilder.insert(index + 2, '-');
                break;
            }
            if (isVowelFollowedByConsonant(word, index) && !isFollowedByLastLetter(word, index)) {
                stringBuilder.insert(index + 1, '-');
                break;
            }
        }
        return stringBuilder.toString();
    }

    private boolean isFollowedByLastLetter(String word, int index) {
        return index + 1 == word.length() - 1;
    }

    private boolean isVowelFollowedBy2Consonant(String word, int index) {
        return isVowelFollowedByConsonant(word, index) && word.length() > index + 2 && !isVoyelle(word.charAt(index + 2));
    }

    private boolean isVowelFollowedByConsonant(String word, int index) {
        return isVoyelle(word.charAt(index))
                && !isVoyelle(word.charAt(index + 1));
    }

    private boolean isVoyelle(char letter) {
        return letter == 'u'
                || letter == 'a'
                || letter == 'e'
                || letter == 'ê'
                || letter == 'i'
                || letter == 'o'
                || letter == 'y';
    }
}
