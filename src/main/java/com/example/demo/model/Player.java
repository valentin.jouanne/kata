package com.example.demo.model;

import java.util.HashMap;
import java.util.Map;

public class Player {

    private Map<String, Integer> abilities;

    public Player() {
        this.abilities = new HashMap<>();
        abilities.put("acrobaties",0);
    }

    public AbilityRoll rollAbility(String ability) {
        if (abilityExist(ability)) {
            return new AbilityRoll(getAbility(ability));
        }
        return null;
    }

    public Integer getAbility(String ability) {
        if (abilityExist(ability)) {
            return abilities.get(ability);
        }
        return null;
    }

    private boolean abilityExist(String ability) {
        return abilities.get(ability) != null;
    }

    public void set(String ability, int value) {
        this.abilities.put(ability, value);
    }
}
