package com.example.demo.model.logigraphe;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ReverseLogigraphe {
    public static final String X = "X";
    private List<String> map;

    public ReverseLogigraphe(List<String> map) {
        this.map = map;
    }

    public ReverseLogigraphe(int[][] map) {
        this.map = Arrays.stream(map).map(this::convert).collect(Collectors.toList());
    }

    public ReverseLogigraphe(BufferedImage image) {
        this.map = Arrays.stream(convertTo2DUsingGetRGB(image)).map(this::convert).collect(Collectors.toList());
    }

    private String convert(int[] v) {
        StringBuilder sb = new StringBuilder();
        for (int b : v) {
            if (b > 0) {
                sb.append("X");
            } else {
                sb.append("-");
            }
        }
        return sb.toString();
    }

    public List<List<Integer>> getHorizontal() {
        List<List<Integer>> horizontale = new ArrayList<>();
        Integer horizontaleLenth = map.size();
        for (int i = 0; i < horizontaleLenth; i++) {
            horizontale.add(getLine(i));
        }
        return horizontale;
    }

    public List<List<Integer>> getVerticale() {
        List<List<Integer>> verticale = new ArrayList<>();
        Integer verticaleLenth = map.get(0).split("").length;
        for (int i = 0; i < verticaleLenth; i++) {
            verticale.add(getColumn(i));
        }
        return verticale;
    }

    private List<Integer> getColumn(Integer index) {
        List<String> column = map.stream().map(v -> v.split("")[index]).collect(Collectors.toList());
        return getValues(column);
    }

    private List<Integer> getLine(Integer index) {
        List<String> line = Arrays.asList(map.get(index).split(""));
        return getValues(line);
    }

    private List<Integer> getValues(List<String> column) {
        if (column.contains(X)) {
            return getSeparatedX(column);
        }
        return Arrays.asList(0);
    }

    private List<Integer> getSeparatedX(List<String> line) {
        List<Integer> separatedX = new ArrayList<>();
        int count = 0;
        for (String v : line) {
            if (v.equals(X)) {
                count++;
            } else if (count > 0) {
                separatedX.add(count);
                count = 0;
            }
        }
        if (count > 0) {
            separatedX.add(count);
        }
        return separatedX;
    }

    private static int[][] convertTo2DUsingGetRGB(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int[][] result = new int[height][width];

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                result[row][col] = image.getRGB(col, row);
            }
        }

        return result;
    }
}
