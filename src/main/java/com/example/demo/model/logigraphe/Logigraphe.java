package com.example.demo.model.logigraphe;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Logigraphe {

    public static final String ERROR = "Error";
    private final List<Integer> horizontale;
    private final List<Integer> verticale;

    public Logigraphe(String verticale, String horizontale) {
        this.verticale = getIntList(verticale);
        this.horizontale = getIntList(horizontale);
    }

    private List<Integer> getIntList(String element) {
        return Arrays.stream(element.split("")).map(Integer::parseInt).collect(Collectors.toList());
    }

    public String result() {

        if (isNullValue(horizontale.get(0))) {
            if (isEmptyLine()) {
                return getEmptyLine();
            }
            return ERROR;
        } else if (isMaxValue(horizontale.get(0))) {
            if (isFullLine()) {
                return getFullLine();
            }
            return ERROR;
        }
        if (verticale.equals(Arrays.asList(0,1))) {
            return "-X";
        }
        return ERROR;

    }

    private boolean isNullValue(Integer line) {
        return line.equals(0);
    }

    private boolean isMaxValue(Integer horizontale) {
        return verticale.size() == horizontale;
    }

    private String getEmptyLine() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < verticale.size(); i++) {
            result.append("-");
        }
        return result.toString();
    }

    private String getFullLine() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < verticale.size(); i++) {
            result.append("X");
        }
        return result.toString();
    }

    private boolean isEmptyLine() {
        return verticale.stream().allMatch(v->v.equals(0));
    }

    private boolean isFullLine() {
        return verticale.stream().allMatch(v->v.equals(1));
    }

}
