package com.example.demo.model;

public class AbilityRoll {

    public static final int DICE_TYPE = 20;
    private final int value;
    private final int roll;

    public AbilityRoll(int value) {
        this.value = value;
        this.roll = new Dice(DICE_TYPE).roll();
    }

    public int getValue() {
        return value;
    }

    public int getRoll() {
        return roll;
    }
}
