package com.example.demo.model;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Dice {
    public static final int MIN_VALUE = 1;
    public static final int DEFAULT_VALUE = 6;
    private static final Logger logger = Logger.getLogger(Dice.class.getName());
    private static final int MAX_REROLL = 10;

    private static final Random random = new Random();
    private final int diceType;
    private final boolean explosive;
    private double reroll;

    public Dice() {
        this.diceType = DEFAULT_VALUE;
        this.explosive = false;
    }

    public Dice(int diceType) {
        this.diceType = diceType;
        this.explosive = false;
    }

    public Dice(int diceType, boolean explosive) {
        this.diceType = diceType;
        this.explosive = explosive;

    }

    public int roll() {
        reroll=0;
        int rollValue = roll1Dice();
        logger.log(Level.INFO, () -> "d" + diceType + " : " + rollValue);
        return rollValue;
    }

    private int roll1Dice() {
        int rollValue = random.nextInt(diceType) + MIN_VALUE;
        if (explosive) {
            return rollValue + getExplodeValue(rollValue);
        }
        return rollValue;
    }

    private int getExplodeValue(int rollValue) {
        int explodeValue = 0;
        if (rollValue == diceType && reroll < MAX_REROLL) {
            reroll++;
            explodeValue += roll1Dice();
        }
        return explodeValue;
    }


}
