package com.example.demo.model.lifeGame;

public class LifeGame {
    public static final String DEAD_CELL = ".";
    public static final String LIVE_CELL = "*";
    Boolean[][] grid;

    public LifeGame(String inputString) {
        String[] lignes = inputString.split("\n");
        int width = lignes[0].split("").length;
        grid = new Boolean[lignes.length][width];

        for (int i = 0; i < lignes.length; i++) {
            String[] cells = lignes[i].split("");
            for (int j = 0; j < cells.length; j++) {
                grid[i][j] = isAlive(cells[j]);
            }
        }
    }

    private boolean isAlive(String cell) {
        return cell.equals(LIVE_CELL);
    }

    public String nextGen() {
        String nextGen = "";
        for (int i = 0; i < grid.length; i++) {
            Boolean[] ligne = grid[i];
            for (int j = 0; j < ligne.length; j++) {
                nextGen += stayAlive(i, j);
            }
            nextGen += "\n";
        }
        return nextGen.substring(0, nextGen.length() - 1);
    }

    private String stayAlive(int i, int j) {
        int lifeAround = lifeCellsAround(i, j);
        if (lifeAround >= 2 && lifeAround < 4) {
            return LIVE_CELL;
        } else {
            return DEAD_CELL;
        }
    }

    private int lifeCellsAround(int i, int j) {
        int lifeCellsAround = 0;
        if (i - 1 >= 0) {
            lifeCellsAround = lifeCellsAroundLine(j, grid[i - 1]);
        }
        if (i + 1 < grid.length) {
            lifeCellsAround = lifeCellsAroundLine(j, grid[i + 1]);
        }

        if (j > 0 && grid[i][j - 1]) {
            lifeCellsAround++;
        }
        if (j < grid[i].length - 1 && grid[i][j + 1]) {
            lifeCellsAround++;
        }
        return lifeCellsAround;
    }

    private int lifeCellsAroundLine(int j, Boolean[] ligne) {
        int lifeCellsAround = 0;
        if (j > 0 && ligne[j - 1]) {
            lifeCellsAround++;
        }
        if (ligne[j]) {
            lifeCellsAround++;
        }
        if (j < ligne.length - 1 && ligne[j + 1]) {
            lifeCellsAround++;
        }
        return lifeCellsAround;
    }
}
