package com.example.demo.model.diamond;

public class Diamond {

    public static final char SPACE = ' ';
    public static final char START_VALUE = 'A';

    public String of(Character letter) {
        return wrapCentralLine(letter);
    }

    private String wrapCentralLine(Character letter) {
        int nbLines = letter - START_VALUE;
        StringBuilder sb = new StringBuilder();
        sb.append(getLine(letter, 0));
        for (int i = 1; i <= nbLines; i++) {
            sb.append("\n").append(getLine(letter, i))
                    .reverse()
                    .append("\n").append(getLine(letter, i));
        }
        return sb.toString();
    }

    private String getLine(Character letter, int index) {
        Character lineChar = getPreviousChar(letter, index);
        String schema = getSchema(lineChar);
        return wrapSpace(schema, index);
    }

    private char getPreviousChar(Character letter, int index) {
        return (char) (letter - index);
    }

    private String wrapSpace(String value, int nbSpace) {
        StringBuilder sb = new StringBuilder();
        sb.append(value);
        for (int i = 0; i < nbSpace; i++) {
            sb.append(SPACE).reverse().append(SPACE);
        }
        return sb.toString();
    }

    private String getSchema(Character letter){
        int nbSpace= (letter - START_VALUE)*2-1;
        StringBuilder sb = new StringBuilder();
        sb.append(letter);
        for (int i = 1; i <= nbSpace; i++) {
            sb.append(SPACE);
        }
        if(nbSpace>0){
            sb.append(letter);
        }
        return sb.toString();
    }
}
