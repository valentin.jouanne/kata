package com.example.demo.model.imageReader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BitmapReader {
    public boolean[][] seeBMPImage(String BMPFileName) throws IOException {
        BufferedImage image = ImageIO.read(new File(BMPFileName));

        boolean[][] array2D = new boolean[image.getWidth()][image.getHeight()];

        for (int xPixel = 0; xPixel < image.getWidth(); xPixel++) {
            for (int yPixel = 0; yPixel < image.getHeight(); yPixel++) {
                int color = image.getRGB(xPixel, yPixel);
                array2D[xPixel][yPixel] = (color == Color.BLACK.getRGB());
            }
        }
        return array2D;
    }
}
