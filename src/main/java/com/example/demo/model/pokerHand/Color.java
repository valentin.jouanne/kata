package com.example.demo.model.pokerHand;

public enum Color {
    H("Heart "),
    S("Spades "),
    D("Diamonds "),
    C("Clubs "),
    ERROR("ERROR ");
    private String value;

    Color(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Color of(Character value) {
        for (Color color : values()) {
            if (color.name().equals(value.toString())) {
                return color;
            }
        }
        return Color.ERROR;
    }
}
