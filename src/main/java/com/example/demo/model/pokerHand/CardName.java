package com.example.demo.model.pokerHand;

public enum CardName {
    JACK("Jack", "J", 11),
    QUEEN("Queen", "Q", 12),
    KING("King", "K", 13),
    ACE("Ace", "A", 14);
    private String value;
    private String charValue;
    private int compareValue;

    CardName(String value, String charValue, int compareValue) {
        this.value = value;
        this.charValue = charValue;
        this.compareValue = compareValue;
    }

    public String getValue() {
        return value;
    }

    public String getChar() {
        return charValue;
    }

    public int getCompareValue() {
        return compareValue;
    }

    static CardName get(String value) {
        for (CardName cardName : values()) {
            if (cardName.getChar().equals(value)) {
                return cardName;
            }
        }
        return null;
    }

    static CardName get(int compareValue) {
        for (CardName cardName : values()) {
            if (cardName.getCompareValue() == (compareValue)) {
                return cardName;
            }
        }
        return null;
    }
}
