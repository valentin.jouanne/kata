package com.example.demo.model.pokerHand;

import java.util.*;
import java.util.stream.Collectors;

public enum Combination {
    HIGH_CARD("high card: ") {
        @Override
        public String getResult() {
            return getMaxCardValue("");

        }

        @Override
        boolean hasCondition(List<Card> cards) {
            return sameCards(0, 0)
                    && !isStraight(cards)
                    && !isFlush(cards);
        }
    },

    PAIR("Pair: ") {
        @Override
        public String getResult() {
            return getCollect("");
        }

        @Override
        boolean hasCondition(List<Card> cards) {
            return sameCards(1, 2);
        }
    },

    TWO_PAIR("Two Pair: ") {
        @Override
        public String getResult() {
            return getCollect(" and ");
        }

        @Override
        boolean hasCondition(List<Card> cards) {
            return sameCards(2, 2);
        }
    },

    THREE_OF_KIND("Three of a kind: ") {
        @Override
        public String getResult() {
            return getCollect("");
        }

        @Override
        boolean hasCondition(List<Card> cards) {
            return sameCards(1, 3);
        }
    },

    FULL_HOUSE("Full house: ") {
        @Override
        public String getResult() {
            return getCollect(" over ");
        }

        @Override
        protected boolean hasCondition(List<Card> cards) {
            return getSameValueCards().containsValue(2)
                    && getSameValueCards().containsValue(3);
        }
    },

    FOUR_OF_KIND("Four of kind: ") {
        @Override
        public String getResult() {
            return getCollect("");
        }

        @Override
        protected boolean hasCondition(List<Card> cards) {
            return getSameValueCards().containsValue(4);
        }
    },

    STRAIGHT("Straight: ") {
        @Override
        public String getResult() {
            return getStraightMaxCardValue("") + "-high";
        }

        @Override
        protected boolean hasCondition(List<Card> cards) {
            return isStraight(cards) && !isFlush(cards);
        }
    },

    FLUSH("Flush: ") {
        @Override
        public String getResult() {
            return getMaxCardValue(getColor()) + "-high";
        }

        @Override
        protected boolean hasCondition(List<Card> cards) {
            return isFlush(cards) && !isStraight(cards);
        }
    },

    STRAIGHT_FLUSH("Straight flush: ") {
        @Override
        public String getResult() {
            return getStraightMaxCardValue(getColor()) + "-high";
        }

        @Override
        protected boolean hasCondition(List<Card> cards) {
            return isFlush(cards) && isStraight(cards);
        }
    },

    NONE("none") {
        @Override
        String getResult() {
            return "no combination find";
        }

        @Override
        boolean hasCondition(List<Card> cards) {
            return false;
        }
    };

    private String value;
    private List<Card> cards;

    Combination(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    private Combination setCards(List<Card> cards) {
        this.cards = cards;
        return this;
    }

    abstract String getResult();

    abstract boolean hasCondition(List<Card> cards);

    String getCollect(String joining) {
        return this.value + getSameValueCards().keySet().stream()
                .sorted(Comparator.comparingInt(v -> getSameValueCards().get(v)).reversed())
                .collect(Collectors.joining(joining));
    }

    Map<String, Integer> getSameValueCards() {
        List<String> values = cards.stream().map(Card::getValue).collect(Collectors.toList());
        return values.stream().filter(i -> Collections.frequency(values, i) > 1)
                .sorted(Comparator.comparingInt(c -> Collections.frequency(values, c)))
                .collect(Collectors.toMap(v -> v, v -> Collections.frequency(values, v), (s, a) -> s));
    }

    boolean sameCards(int lots, int nbrCards) {
        return getSameValueCards().size() == lots
                && getSameValueCards().values().stream().allMatch(v -> v == nbrCards);
    }

    String getMaxCardValue(String prefix) {
        return this.getValue() + prefix + getMaxValue();
    }

    String getStraightMaxCardValue(String prefix) {
        if(getAceHasOneMaxValue().equals("5")){
            return this.getValue() + prefix + getAceHasOneMaxValue();
        }
        return getMaxCardValue(prefix);
    }

    private String getMaxValue() {
        try {
            return cards.stream().max(Card::compare).orElseThrow(Exception::new).getValue();
        } catch (Exception e) {
            return "Error : " + e;
        }
    }
    private String getAceHasOneMaxValue() {
        try {
            return cards.stream().map(Card::AceReturnOne).max(Card::compare).orElseThrow(Exception::new).getValue();
        } catch (Exception e) {
            return "Error : " + e;
        }
    }

    boolean isStraight(List<Card> cards) {
        List<Card> sortedCards = cards.stream().sorted(Card::compare).collect(Collectors.toList());
        List<Card> sortedCardsAceHasOne = cards.stream().map(Card::AceReturnOne).sorted(Card::compare).collect(Collectors.toList());
        return isStraightElement(sortedCards, getMaxValue()) ||
                isStraightElement(sortedCardsAceHasOne, getAceHasOneMaxValue());
    }

    private boolean isStraightElement(List<Card> sortedCards, String maxValue) {
        List<String> sortedValues = sortedCards.stream().map(Card::getValue).collect(Collectors.toList());
        for (Card card : sortedCards) {
            if (!card.getValue().equals(maxValue)) {
                if (!sortedValues.contains(card.getNext().getValue())) {
                    return false;
                }
            }
        }
        return true;
    }

    boolean isFlush(List<Card> cards) {
        return getColors(cards).size() == 1;
    }

    String getColor() {
        try {
            return getColors(cards).stream().findFirst().orElseThrow(Exception::new);
        } catch (Exception e) {
            return "Error : " + e;
        }
    }

    private List<String> getColors(List<Card> cards) {
        return cards.stream().map(Card::getColor).distinct().collect(Collectors.toList());
    }

    public static Combination with(List<Card> cards) {
        return Arrays.stream(values())
                .map(v -> v.setCards(cards))
                .filter(setCards -> setCards.hasCondition(cards))
                .findFirst().orElse(Combination.NONE);
    }
}
