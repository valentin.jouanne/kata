package com.example.demo.model.pokerHand;

public class Card {
    //TODO CardName
    private final String value;
    private final Color color;

    public Card(String stringValue) {
        value = stringValue.substring(0, stringValue.length() - 1);
        char value = stringValue.charAt(stringValue.length() - 1);
        color = Color.of(value);
    }

    private Card(String value, Color color) {
        this.value = value;
        this.color = color;
    }


    public Card getNext() {
        String nextValue;
        if (value.matches("[1-9]")) {
            nextValue = String.valueOf(Integer.parseInt(value) + 1);
        } else {
            CardName nextCardName = CardName.get(this.getNumberToCompare() + 1);
            nextValue = nextCardName.getChar();
        }
        return new Card(nextValue, this.color);
    }

    public String getValue() {
        if (isFace()) {
            return CardName.get(value).getValue();
        } else {
            return value;
        }
    }

    public String getColor() {
        return color.getValue();
    }

    private int getNumberToCompare() {
        if (isFace()) {
            return CardName.get(value).getCompareValue();
        } else {
            return Integer.parseInt(getValue());
        }
    }

    private boolean isFace() {
        return !value.matches("[0-9]+");
    }



    public static Card AceReturnOne(Card card) {
        if (card.getNumberToCompare() == 14) {
            String color = card.getColor().substring(0, 1);
            card = new Card("1" + color);
        }
        return card;
    }

    public static int compare(Card card1, Card card2) {
        return Integer.compare(card1.getNumberToCompare(), card2.getNumberToCompare());
    }


}
