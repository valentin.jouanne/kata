package com.example.demo.model.pokerHand;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PokerHand {

    public String handResult(String hand) {
        List<Card> cards = getCards(hand);
        return Combination.with(cards).getResult();

    }

    private List<Card> getCards(String hand) {
        return Arrays.stream(hand.split(" ")).
                map(Card::new).collect(Collectors.toList());
    }
}

