package com.example.demo.model.pokerHand;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class pokerHandTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1)
    void whenGivenOneHand_shouldReturnResult(String player1,String expected) {
        PokerHand pokerHand = new PokerHand();
        String actualValue = pokerHand.handResult(player1);
            assertEquals(expected, actualValue);
    }

}
