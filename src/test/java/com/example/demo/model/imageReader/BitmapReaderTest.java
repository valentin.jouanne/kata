package com.example.demo.model.imageReader;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BitmapReaderTest {

    @Test
    public void testImage1() throws IOException {
        boolean[][] expected = new boolean[][]{
                {true, false},
                {false, true}
        };
        boolean[][] actual = new BitmapReader().seeBMPImage("src/main/resources/1.bmp");
        isArrayEquals(expected, actual);
    }

    @Test
    public void testImage2() throws IOException {
        boolean[][] expected = new boolean[][]{
                {true, false,true},
                {false, true,false}
        };
        boolean[][] actual = new BitmapReader().seeBMPImage("src/main/resources/2.bmp");
        isArrayEquals(expected, actual);
    }

    private void isArrayEquals(boolean[][] expected, boolean[][] actual) {
        for (int i = 0; i < actual.length; i++) {
            boolean[] line = actual[i];
            for (int i1 = 0; i1 < line.length; i1++) {
                assertEquals(expected[i][i1],actual[i][i1]);
            }
        }
    }
}