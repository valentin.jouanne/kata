package com.example.demo.model.LCD;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class NumberToLCDTest {

    @Test
    @DisplayName("when 0 return LCD 0")
    void when0returnLCD0() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                " _ \n" +
                "| |\n" +
                "|_|";
        assertEquals(expected, LCD.convert(0));
    }

    @Test
    @DisplayName("when 1 return LCD 1")
    void when1returnLCD1() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                "   \n" +
                "  |\n" +
                "  |";
        assertEquals(expected, LCD.convert(1));
    }

    @Test
    @DisplayName("when 2 return LCD 2")
    void when2returnLCD2() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                " _ \n" +
                " _|\n" +
                "|_ ";
        assertEquals(expected, LCD.convert(2));
    }

    @Test
    @DisplayName("when 3 return LCD 3")
    void when3returnLCD3() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                " _ \n" +
                " _|\n" +
                " _|";
        assertEquals(expected, LCD.convert(3));
    }

    @Test
    @DisplayName("when 4 return LCD 4")
    void when4returnLCD4() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                "   \n" +
                "|_|\n" +
                "  |";
        assertEquals(expected, LCD.convert(4));
    }

    @Test
    @DisplayName("when 5 return LCD 5")
    void when5returnLCD5() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                " _ \n" +
                "|_ \n" +
                " _|";
        assertEquals(expected, LCD.convert(5));
    }

    @Test
    @DisplayName("when 6 return LCD 6")
    void when6returnLCD6() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                " _ \n" +
                "|_ \n" +
                "|_|";
        assertEquals(expected, LCD.convert(6));
    }

    @Test
    @DisplayName("when 7 return LCD 7")
    void when7returnLCD7() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                " _ \n" +
                "  |\n" +
                "  |";
        assertEquals(expected, LCD.convert(7));
    }

    @Test
    @DisplayName("when 8 return LCD 8")
    void when8returnLCD8() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                " _ \n" +
                "|_|\n" +
                "|_|";
        assertEquals(expected, LCD.convert(8));
    }

    @Test
    @DisplayName("when 9 return LCD 9")
    void when9returnLCD9() {
        NumberToLCD LCD = new NumberToLCD();
        String expected = "" +
                " _ \n" +
                "|_|\n" +
                " _|";
        assertEquals(expected, LCD.convert(9));
    }

}