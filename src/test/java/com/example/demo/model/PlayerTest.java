package com.example.demo.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PlayerTest {


    public static final String NOT_EXISTING_ABILITY = "yoloooo";
    public static final String EXISTING_ABILITY = "acrobaties";
    private Player player;

    @BeforeEach
    void SetUp(){
        player = new Player();
    }
    @Test
    void whenGetNotExistingAbility_ShouldReturnNull(){
        Assertions.assertNull(player.getAbility(NOT_EXISTING_ABILITY));
    }

    @Test
    void whenGetExistingAbility_ShouldReturAbilityScore(){
        Assertions.assertNotNull(player.getAbility(EXISTING_ABILITY));
    }

    @Test
    void whenAbilityPlayerIs0_GetAbilityShouldReturn0(){
        player.set(EXISTING_ABILITY,0);
        Assertions.assertEquals(0,player.getAbility(EXISTING_ABILITY));
    }

    @Test
    void whenAbilityPlayerIs1_GetAbilityShouldReturn1(){
        player.set(EXISTING_ABILITY,1);
        Assertions.assertEquals(1,player.getAbility(EXISTING_ABILITY));
    }

    //////////

    @Test
    void whenGetNotExistingAbilityRoll_ShouldReturnNull(){
        Assertions.assertNull(player.rollAbility(NOT_EXISTING_ABILITY));
    }

    @Test
    void whenGetExistingAbilityRoll_ShouldReturnNotNull(){
        Assertions.assertNotNull(player.rollAbility(EXISTING_ABILITY));
    }

    @Test
    void whenGetExistingAbilityRoll_ShouldReturnAbilityValue(){
        player.set(EXISTING_ABILITY,42);
        AbilityRoll actual = player.rollAbility(EXISTING_ABILITY);
        Assertions.assertEquals(42,actual.getValue());
    }

    @Test
    void whenGetExistingAbilityRoll_ShouldReturnAbilityRoll(){
        AbilityRoll actual = player.rollAbility(EXISTING_ABILITY);
        Assertions.assertTrue(actual.getRoll()<20);
    }
}