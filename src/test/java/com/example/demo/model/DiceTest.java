package com.example.demo.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class DiceTest {

    @Test
    void whenRollDiceMinValueShouldBe1(){
        Assertions.assertEquals(1, getLargeRollsValues(new Dice()).iterator().next());
    }

    @Test
    void whenRollDiceShouldHave6Values(){
        Assertions.assertEquals(6, getLargeRollsValues(new Dice()).size());
    }

    @Test
    void whenRollDiceMaxValueShouldBe6(){
        Integer maxValue = getLargeRollsValues(new Dice()).stream().max(Integer::compare).orElse(0);
        Assertions.assertEquals(6, maxValue);
    }
    @Test
    void whenRollDice4MinValueShouldBe1(){
        Dice d4 = new Dice(4);
        Assertions.assertEquals(1, getLargeRollsValues(d4).iterator().next());
    }
    @Test
    void whenRollDice4ShouldHave4Values(){
        Dice d4 = new Dice(4);
        Assertions.assertEquals(4, getLargeRollsValues(d4).size());
    }

    @Test
    void whenRollDice4MaxValueShouldBe4(){
        Dice d4 = new Dice(4);
        Integer maxValue = getLargeRollsValues(d4).stream().max(Integer::compare).orElse(0);
        Assertions.assertEquals(4, maxValue);
    }

    @Test
    void whenRollExplosiveDice20MaxValueShouldBeOver20(){
        Dice d20 = new Dice(20, true);
        Integer maxValue = getLargeRollsValues(d20).stream().max(Integer::compare).orElse(0);
        Assertions.assertTrue(maxValue>20);
    }
    @Test
    void whenRollExplosiveDice1MaxValueShouldBe11(){
        Dice d1 = new Dice(1, true);
        Integer maxValue = getLargeRollsValues(d1).stream().max(Integer::compare).orElse(0);
        Assertions.assertEquals(11,maxValue);
    }


    private Set<Integer> getLargeRollsValues(Dice d6) {
        Set<Integer> rolls = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            rolls.add(d6.roll());
        }
        return rolls;
    }

}