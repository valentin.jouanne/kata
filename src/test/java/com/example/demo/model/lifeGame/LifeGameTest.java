package com.example.demo.model.lifeGame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LifeGameTest {

    @Test
    @DisplayName("grid 1*1 dead cell stay dead")
    public void test1(){
        //given
        String inputString = ".";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals(".",gen2);
    }

    @Test
    @DisplayName("grid 1*1 live cell become dead")
    public void test2(){
        //given
        String inputString = "*";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals(".",gen2);
    }

    @Test
    @DisplayName(".. => ..")
    public void test3(){
        //given
        String inputString = "..";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals("..",gen2);
    }

    @Test
    @DisplayName("*. => ..")
    public void test4(){
        //given
        String inputString = "*.";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals("..",gen2);
    }

    @Test
    @DisplayName("** => ..")
    public void test5(){
        //given
        String inputString = "**";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals("..",gen2);
    }

    @Test
    @DisplayName("... => ...")
    public void test6(){
        //given
        String inputString = "...";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals("...",gen2);
    }

    @Test
    @DisplayName(".*. => ...")
    public void test7(){
        //given
        String inputString = ".*.";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals("...",gen2);
    }

    @Test
    @DisplayName("*** => .*.")
    public void test8(){
        //given
        String inputString = "***";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals(".*.",gen2);
    }

    @Test
    @DisplayName("**** => .**.")
    public void test9(){
        //given
        String inputString = "****";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals(".**.",gen2);
    }

    @Test
    @DisplayName("***** => .***.")
    public void test10(){
        //given
        String inputString = "*****";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals(".***.",gen2);
    }

    @Test
    @DisplayName("./n. => ./n.")
    public void test11(){
        //given
        String inputString =
                ".\n" +
                ".";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals(".\n.",gen2);
    }

    @Test
    @DisplayName("../n.. => ../n..")
    public void test12(){
        //given
        String inputString =
                "..\n" +
                "..";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals("..\n..",gen2);
    }

    @Test
    @DisplayName("**/n** => **/n**")
    public void test13(){
        //given
        String inputString =
                "**\n" +
                "**";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        assertEquals("**\n**",gen2);
    }

    @Test
    @DisplayName("***/n***/n*** => *.*/n.../n*.*")
    public void test14(){
        //given
        String inputString =
                "***\n" +
                "***\n" +
                "***";
        LifeGame lifegame = new LifeGame(inputString);
        //when
        String gen2 = lifegame.nextGen();
        //then
        String expected =
                "*.*\n" +
                "...\n" +
                "*.*";
        assertEquals(expected,gen2);
    }
}