package com.example.demo.model.diamond;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DiamondTest {
    private Diamond diamond;

    @BeforeEach
    void setUp() {
        diamond = new Diamond();
    }

    @Test
    void whenA_ReturnA() {
        assertEquals("A", diamond.of('A'));
    }

    @Test
    void whenB_ReturnABBA() {
        String expected = "" +
                " A \n" +
                "B B\n" +
                " A ";
        assertEquals(expected, diamond.of('B'));
    }

    @Test
    void whenC_ReturnABCCBA() {
        String expected = "" +
                "  A  \n" +
                " B B \n" +
                "C   C\n" +
                " B B \n" +
                "  A  ";
        assertEquals(expected, diamond.of('C'));
    }

    @Test
    void whenD_ReturnABCDDCBA() {
        String expected = "" +
                "   A   \n" +
                "  B B  \n" +
                " C   C \n" +
                "D     D\n" +
                " C   C \n" +
                "  B B  \n" +
                "   A   ";
        assertEquals(expected, diamond.of('D'));
    }

}