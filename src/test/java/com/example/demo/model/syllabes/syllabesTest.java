package com.example.demo.model.syllabes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class syllabesTest {

    public static final String LE_MOT_A_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_N_AURAIT_PAS_DÛ = "le mot a été découpé alors qu'il n'aurait pas dû";
    public static final String LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ = "le mot n'a pas été découpé alors qu'il aurait dû";
    private final Syllabes syllabes  = new Syllabes();

    @Test
    @DisplayName("Si le mot null => null")
    void whenNull_shouldReturnNull(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate(null);
        //THEN
        assertEquals(null, actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot '' => ''")
    void whenEmpty_shouldReturnEmpty(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("");
        //THEN
        assertEquals("", actual, "un mot vide doit renvoyer vide");
    }
    @Test
    @DisplayName("Si le mot 'à' n'est pas découpé")
    void whenA_shouldNoSeperate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("à");
        //THEN
        assertEquals("à", actual, LE_MOT_A_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_N_AURAIT_PAS_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'il' n'est pas découpé")
    void whenIl_shouldNoSeperate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("il");
        //THEN
        assertEquals("il", actual, LE_MOT_A_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_N_AURAIT_PAS_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'fou' n'est pas découpé")
    void whenFou_noSeparator(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("fou");
        //THEN
        assertEquals("fou", actual, LE_MOT_A_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_N_AURAIT_PAS_DÛ);
    }

    @Test
    @DisplayName("Si le mot loup n'est pas découpé")
    void whenLoup_shouldNoSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("loup");
        //THEN
        assertEquals("loup", actual, LE_MOT_A_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_N_AURAIT_PAS_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'coupant' => 'cou-pant'")
    void whenCoupant_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("coupant");
        //THEN
        assertEquals("cou-pant", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'sapin' => 'sa-pin'")
    void whenSapin_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("sapin");
        //THEN
        assertEquals("sa-pin", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'benêt' => 'be-nêt'")
    void whenBenet_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("benêt");
        //THEN
        assertEquals("be-nêt", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'galop' => 'ga-lop'")
    void whenGalop_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("galop");
        //THEN
        assertEquals("ga-lop", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'maison' => 'mai-son'")
    void whenMaison_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("maison");
        //THEN
        assertEquals("mai-son", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'peinte' => 'pein-te'")
    void whenPeinte_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("peinte");
        //THEN
        assertEquals("pein-te", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'marteau' => 'mar-teau'")
    void whenMarteau_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("marteau");
        //THEN
        assertEquals("mar-teau", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'pelle' => 'pel-le'")
    void whenPelle_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("pelle");
        //THEN
        assertEquals("pel-le", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'voyelle' => 'voyel-le'")
    void whenVoyelle_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("voyelle");
        //THEN
        assertEquals("voyel-le", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }

    @Test
    @DisplayName("Si le mot 'train' => 'train'")
    void whenTrain_shouldSeparate(){
        //GIVEN
        //WHEN
        String actual = syllabes.separate("train");
        //THEN
        assertEquals("train", actual, LE_MOT_N_A_PAS_ÉTÉ_DÉCOUPÉ_ALORS_QU_IL_AURAIT_DÛ);
    }




}