package com.example.demo.model.logigraphe;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LogigrapheTest {

    public static final String ERROR = "Error";

    @Test
    void when0_0_ShouldReturn_() {
        assertEquals("-", new Logigraphe("0", "0").result());
    }

    @Test
    void when0_1_ShouldReturnError() {
        assertEquals(ERROR, new Logigraphe("0", "1").result());
    }

    @Test
    void when1_0_ShouldReturnError() {
        assertEquals(ERROR, new Logigraphe("1", "0").result());
    }

    @Test
    void when1_1_ShouldReturnX() {
        assertEquals("X", new Logigraphe("1", "1").result());
    }

    @Test
    void when00_0_ShouldReturn__() {
        assertEquals("--", new Logigraphe("00", "0").result());
    }

    @Test
    void when000_0_ShouldReturn___() {
        assertEquals("---", new Logigraphe("000", "0").result());
    }

    @Test
    void when0000_0_ShouldReturn___() {
        assertEquals("----", new Logigraphe("0000", "0").result());
    }

    @Test
    void when00_1_ShouldReturnError() {
        assertEquals(ERROR, new Logigraphe("00", "1").result());
    }

    @Test
    @DisplayName("when 11/2 should return XX")
    void when11_2_ShouldReturnXX() {
        assertEquals("XX", new Logigraphe("11", "2").result());
    }

    @Test
    @DisplayName("when 111/3 should return XXX")
    void when111_3_ShouldReturnXXX() {
        assertEquals("XXX", new Logigraphe("111", "3").result());
    }

    @Test
    @DisplayName("when 01/1 should return -X")
    void when01_1_ShouldReturn_X() {
        assertEquals("-X", new Logigraphe("01", "1").result());
    }
}