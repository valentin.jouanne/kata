package com.example.demo.model.logigraphe;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

class ReverseLogigrapheTest {

    @Test
    void testHorizontaleTable() {
        int[][] map = new int[][]{
                {0, 1, 1, 0, 1, 1, 0},
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0}
        };
        List<List<Integer>> expected = Arrays.asList(
                Arrays.asList(2, 2),
                Arrays.asList(1, 1, 1),
                Arrays.asList(1, 1),
                Arrays.asList(1, 1),
                Arrays.asList(1));
        Assertions.assertEquals(expected, new ReverseLogigraphe(map).getHorizontal());
    }

    @Test
    void testVerticaleTable() {
        int[][] map = new int[][]{
                {0, 1, 1, 0, 1, 1, 0},
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0}
        };
        List<List<Integer>> expected = Arrays.asList(
                Arrays.asList(1),
                Arrays.asList(1, 1),
                Arrays.asList(1, 1),
                Arrays.asList(1, 1),
                Arrays.asList(1, 1),
                Arrays.asList(1, 1),
                Arrays.asList(1));
        Assertions.assertEquals(expected, new ReverseLogigraphe(map).getVerticale());
    }

    @ParameterizedTest(name = "when map is {0} horizontale should return {1}")
    @MethodSource("dataProviderHorizontale")
    void testHorizontale(List<String> map, List<List<Integer>> expected) {
        Assertions.assertEquals(expected, new ReverseLogigraphe(map).getHorizontal());
    }

    @ParameterizedTest(name = "when map is {0} verticale should return {1}")
    @MethodSource("dataProviderVerticale")
    void testVerticale(List<String> map, List<List<Integer>> expected) {
        Assertions.assertEquals(expected, new ReverseLogigraphe(map).getVerticale());
    }

    private static Stream<Arguments> dataProviderVerticale() {
        return Stream.of(
                Arguments.of(Collections.singletonList("-"), Collections.singletonList(Collections.singletonList(0))),
                Arguments.of(Collections.singletonList("X"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Arrays.asList("X", "X"), Collections.singletonList(Collections.singletonList(2))),
                Arguments.of(Arrays.asList("-", "X"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Arrays.asList("X", "-"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Arrays.asList("-", "-"), Collections.singletonList(Collections.singletonList(0))),
                Arguments.of(Arrays.asList("-", "-", "X", "X", "-", "X"), Collections.singletonList(Arrays.asList(2, 1))),
                Arguments.of(Collections.singletonList("-X"), Arrays.asList(Collections.singletonList(0), Collections.singletonList(1))),
                Arguments.of(Arrays.asList(
                        "-XX-XX-",
                        "X--X--X",
                        "-X---X-",
                        "--X-X--",
                        "---X---"),
                        Arrays.asList(Arrays.asList(1), Arrays.asList(1, 1), Arrays.asList(1, 1), Arrays.asList(1, 1), Arrays.asList(1, 1), Arrays.asList(1, 1), Arrays.asList(1)))
        );
    }

    private static Stream<Arguments> dataProviderHorizontale() {
        return Stream.of(
                Arguments.of(Collections.singletonList("-"), Collections.singletonList(Collections.singletonList(0))),
                Arguments.of(Collections.singletonList("X"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Collections.singletonList("--"), Collections.singletonList(Collections.singletonList(0))),
                Arguments.of(Collections.singletonList("X-"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Collections.singletonList("-X"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Collections.singletonList("XX"), Collections.singletonList(Collections.singletonList(2))),
                Arguments.of(Collections.singletonList("---"), Collections.singletonList(Collections.singletonList(0))),
                Arguments.of(Collections.singletonList("X--"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Collections.singletonList("-X-"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Collections.singletonList("--X"), Collections.singletonList(Collections.singletonList(1))),
                Arguments.of(Collections.singletonList("XX-"), Collections.singletonList(Collections.singletonList(2))),
                Arguments.of(Collections.singletonList("-XX"), Collections.singletonList(Collections.singletonList(2))),
                Arguments.of(Collections.singletonList("XXX"), Collections.singletonList(Collections.singletonList(3))),
                Arguments.of(Collections.singletonList("X-X"), Collections.singletonList(Arrays.asList(1, 1))),
                Arguments.of(Collections.singletonList("X-X-"), Collections.singletonList(Arrays.asList(1, 1))),
                Arguments.of(Collections.singletonList("-X-X"), Collections.singletonList(Arrays.asList(1, 1))),
                Arguments.of(Collections.singletonList("X--X"), Collections.singletonList(Arrays.asList(1, 1))),
                Arguments.of(Collections.singletonList("X-XX"), Collections.singletonList(Arrays.asList(1, 2))),
                Arguments.of(Collections.singletonList("XX-X"), Collections.singletonList(Arrays.asList(2, 1))),
                Arguments.of(Collections.singletonList("--XX-X-XXX---XX--X---XX--"), Collections.singletonList(Arrays.asList(2, 1, 3, 2, 1, 2))),
                Arguments.of(Arrays.asList(
                        "-XX-XX-",
                        "X--X--X",
                        "-X---X-",
                        "--X-X--",
                        "---X---"),
                        Arrays.asList(Arrays.asList(2, 2), Arrays.asList(1, 1, 1), Arrays.asList(1, 1), Arrays.asList(1, 1), Arrays.asList(1)))
        );


    }
}